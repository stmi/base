# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.4 [2022-02-04]
- Add Google Guava

## 1.0.3 [2022-01-29]
- Add Apache Math3

## 1.0.2 [2022-01-28]
- Upgrade Java version to 17
- Update dependencies versions in properties
- Add README sections on using 

## 1.0.1 [2021-09-28]
- Upgrade JQwik to 1.5.6

## 1.0 [2021-09-26]
### Added
- Default to Java 16
- DependencyManagement section with:
  - JUnit 5
  - JQwik
  - AssertJ
  - ArchUnit
  - Lombok
  - Mockito
- GitLab CI with package publishing to gitlab
- Maven lifecycle plugins in highest available versions
- Lombok annotation processor in compiler config
- JaCoCo coverage check in build pluginManagement and plugins
- License MIT