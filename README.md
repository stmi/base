# Mvn Base

Parent project for building personal java projects - shared plugins, libs and settings

## Checking and updating versions of plugins

[Versions Maven Plugin](https://www.mojohaus.org/versions-maven-plugin/)
used for automatic bumping of versions

* Check new available versions:  
  `mvn versions:display-dependency-updates`

* Update versions defined in properties:  
  `mvn versions:update-properties`
